from time import sleep

from main import foo


def test_quick():
    assert 1 == True


def test_foo():
    assert foo() == "bar"


def test_slow():
    print("\nStart slow test")
    sleep(5)
    assert 1 == True
    print("\nEnd slow test")
